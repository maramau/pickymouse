#!/usr/bin/python3

import pyautogui as g
import sys
from time import sleep
import yaml
from actions import actions
act = actions()

if len(sys.argv) > 1:
    workflowFile=str(sys.argv[1]) 
else:
    workflowFile='workflow.yaml'    


with open(workflowFile) as file:
    try:
        workflow = yaml.safe_load(file)   
        print(workflow)
    except yaml.YAMLError as exc:
        print(exc)

#TODO: auto ricerca del posto dove cliccare ->print(g.locateOnScreen('images/minivideo/guida.png'))
size=workflow.pop('layout')

for move in workflow:
    print(move)
    print(workflow[move])
    act.doAction(workflow[move]['action'],workflow[move]['arguments'])
    
    
    
    