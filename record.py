#!/usr/bin/python3
# python3 -m pip install pynput
from pynput import mouse
import pyautogui as g
import sys

import yaml

import logging
clics = 0
step = 0
point = []
if len(sys.argv) > 1:
    workflowFile=str(sys.argv[1]) 
else:
    workflowFile='workflow.yml'    
print(workflowFile)

logging.basicConfig(filename="mouse_log.txt", level=logging.DEBUG, format='%(asctime)s: %(message)s')
with open(workflowFile) as file:
    try:
        workflow = yaml.safe_load(file)   
        print(workflow)
    except yaml.YAMLError as exc:
        print(exc)



def on_move(x, y):
    logging.info("Mouse moved to ({0}, {1})".format(x, y))

def on_click(x, y, button, pressed):
    global clics,point
    if pressed :
        print(clics,step)
        clics += 1
        if clics == (step +1):
            print('Mouse clicked at ({0}, {1}) with {2}'.format(x, y, button))
            listener.stop()
            clics= 0
            point=[x,y]
            print(point)
            return [x,y]

def on_scroll(x, y, dx, dy):
    logging.info('Mouse scrolled at ({0}, {1})({2}, {3})'.format(x, y, dx, dy))
str=''    
for index,move_key in enumerate(workflow):
    step = index
    # inserisci qui i movimenti registrati fino a questo momento
    
    str='{0}\n{1}'.format(str , move_key)
    print(str)
    g.alert('clicca su {}'.format(str),'clicca')


    with mouse.Listener(on_move=on_move, on_click=on_click, on_scroll=on_scroll) as listener:
        listener.join()

    workflow[move_key]['position']['x']=point[0]
    workflow[move_key]['position']['y']=point[1]
print(workflow)    
            
with open('recorded.yaml', 'w') as file:

    outputs = yaml.dump(workflow, file)
    print(outputs)

    