# pickymouse
In place self movement mouse tutorial.

## Getting started
To see a simple task performed by this nice app you have simply to lauch the app:
cursor begins to move by itself, performing some simple actions.
There are only few important file in this project:
*.yaml  is the sequence description for atomic actions
actions.py is the basic class that interact with the cursor
record.py record all important mouse actions
minivideo.py moves cursor according to *.yaml

## Installation
simply clone the project and adapt *.yaml to your needing

## Usage
*.yaml is a list of blocks, each one begins with its name and contains specific options.
The **layout** block describe the behavior in which the file was recorded or intended to use to.
When you want add an action you have to add a block named as you want and add at least 
- a sub-item named **action** and declared choosing the right word.
- a sub-item named **arguments** filled with the right keyword according to **action** above
optionally you may add a sub-item named **time** that specify how much seconds, or fractions, the action will take to end


## Roadmap
- Now the project is record specific: it doesn't care of different behavior of DE,window themes library.... it simply take care of pixel coordinates of monitor in which was recorded so we will add a tool to transform from one to another.
- we can add a simple gui to create the list of actions with accordings arguments
.....

## Contributing
You are invited to contribute as you can

## Authors and acknowledgment
At the moment, the very begin, I'm the only contributor
## License
GPLv3
