import pyautogui as g
from pynput.mouse import Button, Controller
from time import sleep

mouse = Controller()

dictionary={}

class actions:
    """ questa classe definisce un metodo per ogni azione che voglio intraprendere
        senza considerare le dimensioni dello schermo,per questo c'è un metodo apposito
        transformCoord"""
    def __init__(self,size=g.size()):
        self.size=size
        
    def transformCoord(self,coord,size1):
        #TODO
        pass
        
        
    def doAction(self,actionName,*args):
        print('chiamata: "{}" con argomenti:'.format(actionName))
        for arg in args:
            print('- {}'.format(arg))
        method = getattr(self,actionName, lambda: 'invalide choise')
        return method(*args)
    def moveTo(self,x,y,menu=0):
        print(menu)
        point=g.position()
        dict={
            0 : [x,y],
            1 : [None,y],
            2 : [x,None]
        }
        print('ecco il punto: ({};{})'.format( dict.get(menu)[0],dict.get(menu)[1]))
        g.moveTo(dict.get(menu)[0],dict.get(menu)[1],2,g.easeInOutQuad)
        
    def click(self,arg):
        x=arg['position']['x']
        y=arg['position']['y']
        menu=arg['menu']
        self.moveTo(x,y,menu)
        g.leftClick()
        sleep(1)
    def write(self,args):
        text=args['text']
        g.write(text,0.1,)
    def drag(self,args):
        fromX=args['from']['x']
        fromY=args['from']['y']
        toX=args['to']['x']
        toY=args['to']['y']
        self.moveTo(fromX,fromY)
        g.dragTo(toX,toY,2)
        
    def sayHello(self,s,d):
        print('hello!',s,d)
        